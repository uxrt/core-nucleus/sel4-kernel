/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once
#include <sel4/mode/types.h>
typedef enum {
    seL4_NoError = 0,
    seL4_InvalidArgument,
    seL4_InvalidCapability,
    seL4_IllegalOperation,
    seL4_RangeError,
    seL4_AlignmentError,
    seL4_FailedLookup,
    seL4_TruncatedMessage,
    seL4_DeleteFirst,
    seL4_RevokeFirst,
    seL4_NotEnoughMemory,
    seL4_InvalidLocalIPCAddress,
    seL4_InvalidRemoteIPCAddress,
    seL4_ReplySequenceError,
    seL4_EndpointCancelled,
    seL4_ThreadCancelled,

    /* This should always be the last item in the list
     * so it gives a count of the number of errors in the
     * enum.
     */
    seL4_NumErrors
} seL4_Error;

#define seL4_IsTransferError LIBSEL4_BIT(seL4_LabelBits - 1)
#define seL4_MinTransferError seL4_InvalidLocalIPCAddress
#define seL4_MaxTransferError seL4_ThreadCancelled
