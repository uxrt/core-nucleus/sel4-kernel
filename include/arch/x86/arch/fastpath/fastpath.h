/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <mode/fastpath/fastpath.h>

void slowpath(syscall_t syscall)
NORETURN;

void fastpath_call(word_t cptr, word_t r_msgInfo)
NORETURN;

void fastpath_reply_recv(word_t cptr, word_t r_msgInfo, word_t reply)
NORETURN;
