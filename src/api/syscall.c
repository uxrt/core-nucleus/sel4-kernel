/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <config.h>
#include <types.h>
#include <benchmark/benchmark.h>
#include <arch/benchmark.h>
#include <benchmark/benchmark_track.h>
#include <benchmark/benchmark_utilisation.h>
#include <api/syscall.h>
#include <api/failures.h>
#include <api/faults.h>
#include <kernel/cspace.h>
#include <kernel/faulthandler.h>
#include <kernel/thread.h>
#include <kernel/vspace.h>
#include <machine/io.h>
#include <plat/machine/hardware.h>
#include <object/interrupt.h>
#include <model/statedata.h>
#include <string.h>
#include <kernel/traps.h>
#include <arch/machine.h>
#ifdef ENABLE_SMP_SUPPORT
#include <smp/ipi.h>
#endif
#ifdef CONFIG_DEBUG_BUILD
#include <arch/machine/capdl.h>
#endif

/* The haskell function 'handleEvent' is split into 'handleXXX' variants
 * for each event causing a kernel entry */

exception_t handleInterruptEntry(void)
{
    irq_t irq;

    irq = getActiveIRQ();
    if (SMP_TERNARY(clh_is_self_in_queue(), 1)) {
        updateTimestamp();
        checkBudget();
    }

    if (IRQT_TO_IRQ(irq) != IRQT_TO_IRQ(irqInvalid)) {
        handleInterrupt(irq);
    } else {
#ifdef CONFIG_IRQ_REPORTING
        userError("Spurious interrupt!");
#endif
        handleSpuriousIRQ();
    }

    if (SMP_TERNARY(clh_is_self_in_queue(), 1)) {
        schedule();
        activateThread();
    }

    return EXCEPTION_NONE;
}

exception_t handleUnknownSyscall(word_t w)
{
#ifdef CONFIG_PRINTING
    if (w == SysDebugPutChar) {
        kernel_putchar(getRegister(NODE_STATE(ksCurThread), capRegister));
        return EXCEPTION_NONE;
    }
    if (w == SysDebugDumpScheduler) {
#ifdef CONFIG_DEBUG_BUILD
        debug_dumpScheduler();
#endif
        return EXCEPTION_NONE;
    }
#endif
#ifdef CONFIG_DEBUG_BUILD
    if (w == SysDebugHalt) {
        /* TODO: make this dependent on a capability (possibly the initial thread?) to make sure that it can't be called by an unauthorized thread */
        tcb_t *UNUSED tptr = NODE_STATE(ksCurThread);
        printf("Debug halt syscall from user thread %p \"%s\"\n", tptr, TCB_PTR_DEBUG_PTR(tptr)->tcbName);
        halt();
    }
    if (w == SysDebugSnapshot) {
        tcb_t *UNUSED tptr = NODE_STATE(ksCurThread);
        printf("Debug snapshot syscall from user thread %p \"%s\"\n",
               tptr, TCB_PTR_DEBUG_PTR(tptr)->tcbName);
        debug_capDL();
        return EXCEPTION_NONE;
    }
    if (w == SysDebugCapIdentify) {
        word_t cptr = getRegister(NODE_STATE(ksCurThread), capRegister);
        lookupCapAndSlot_ret_t lu_ret = lookupCapAndSlot(NODE_STATE(ksCurThread), cptr);
        word_t cap_type = cap_get_capType(lu_ret.cap);
        setRegister(NODE_STATE(ksCurThread), capRegister, cap_type);
        return EXCEPTION_NONE;
    }

    if (w == SysDebugNameThread) {
        /* This is a syscall meant to aid debugging, so if anything goes wrong
         * then assume the system is completely misconfigured and halt */
        const char *name;
        word_t len;
        word_t cptr = getRegister(NODE_STATE(ksCurThread), capRegister);
        lookupCapAndSlot_ret_t lu_ret = lookupCapAndSlot(NODE_STATE(ksCurThread), cptr);
        /* TODO: return an error here */
        /* ensure we got a TCB cap */
        word_t cap_type = cap_get_capType(lu_ret.cap);
        if (cap_type != cap_thread_cap) {
            userError("SysDebugNameThread: cap is not a TCB, halting");
            halt();
        }
        /* Add 1 to the IPC buffer to skip the message info word */
        name = (const char *)(lookupIPCBuffer(true, NODE_STATE(ksCurThread)) + 1);
        if (!name) {
            userError("SysDebugNameThread: Failed to lookup IPC buffer, halting");
            halt();
        }
        /* ensure the name isn't too long */
        len = strnlen(name, seL4_MsgMaxLength * sizeof(word_t));
        if (len == seL4_MsgMaxLength * sizeof(word_t)) {
            userError("SysDebugNameThread: Name too long, halting");
            halt();
        }
        setThreadName(TCB_PTR(cap_thread_cap_get_capTCBPtr(lu_ret.cap)), name);
        return EXCEPTION_NONE;
    }
#ifdef ENABLE_SMP_SUPPORT
    if (w == SysDebugSendIPI) {
        return handle_SysDebugSendIPI();
    }
#endif /* ENABLE_SMP_SUPPORT */
#endif /* CONFIG_DEBUG_BUILD */

#ifdef CONFIG_DANGEROUS_CODE_INJECTION
    if (w == SysDebugRun) {
        ((void (*)(void *))getRegister(NODE_STATE(ksCurThread), capRegister))((void *)getRegister(NODE_STATE(ksCurThread),
                                                                                                  msgInfoRegister));
        return EXCEPTION_NONE;
    }
#endif

#ifdef CONFIG_KERNEL_X86_DANGEROUS_MSR
    if (w == SysX86DangerousWRMSR) {
        uint64_t val;
        uint32_t reg = getRegister(NODE_STATE(ksCurThread), capRegister);
        if (CONFIG_WORD_SIZE == 32) {
            val = (uint64_t)getSyscallArg(0, NULL) | ((uint64_t)getSyscallArg(1, NULL) << 32);
        } else {
            val = getSyscallArg(0, NULL);
        }
        x86_wrmsr(reg, val);
        return EXCEPTION_NONE;
    } else if (w == SysX86DangerousRDMSR) {
        uint64_t val;
        uint32_t reg = getRegister(NODE_STATE(ksCurThread), capRegister);
        val = x86_rdmsr(reg);
        int num = 1;
        if (CONFIG_WORD_SIZE == 32) {
            setMR(NODE_STATE(ksCurThread), NULL, 0, val & 0xffffffff);
            setMR(NODE_STATE(ksCurThread), NULL, 1, val >> 32);
            num++;
        } else {
            setMR(NODE_STATE(ksCurThread), NULL, 0, val);
        }
        setRegister(NODE_STATE(ksCurThread), msgInfoRegister, wordFromMessageInfo(seL4_MessageInfo_new(0, 0, 0, num)));
        return EXCEPTION_NONE;
    }
#endif

#ifdef CONFIG_ENABLE_BENCHMARKS
    switch (w) {
    case SysBenchmarkFlushCaches:
        return handle_SysBenchmarkFlushCaches();
    case SysBenchmarkResetLog:
        return handle_SysBenchmarkResetLog();
    case SysBenchmarkFinalizeLog:
        return handle_SysBenchmarkFinalizeLog();
#ifdef CONFIG_ENABLE_KERNEL_LOG_BUFFER
    case SysBenchmarkSetLogBuffer:
        return handle_SysBenchmarkSetLogBuffer();
#endif /* CONFIG_ENABLE_KERNEL_LOG_BUFFER */
#ifdef CONFIG_BENCHMARK_TRACK_UTILISATION
    case SysBenchmarkGetThreadUtilisation:
        return handle_SysBenchmarkGetThreadUtilisation();
    case SysBenchmarkResetThreadUtilisation:
        return handle_SysBenchmarkResetThreadUtilisation();
#ifdef CONFIG_DEBUG_BUILD
    case SysBenchmarkDumpAllThreadsUtilisation:
        return handle_SysBenchmarkDumpAllThreadsUtilisation();
    case SysBenchmarkResetAllThreadsUtilisation:
        return handle_SysBenchmarkResetAllThreadsUtilisation();
#endif /* CONFIG_DEBUG_BUILD */
#endif /* CONFIG_BENCHMARK_TRACK_UTILISATION */
    case SysBenchmarkNullSyscall:
        return EXCEPTION_NONE;
    default:
        break; /* syscall is not for benchmarking */
    } /* end switch(w) */
#endif /* CONFIG_ENABLE_BENCHMARKS */

    MCS_DO_IF_BUDGET({
#ifdef CONFIG_SET_TLS_BASE_SELF
        if (w == SysSetTLSBase)
        {
            word_t tls_base = getRegister(NODE_STATE(ksCurThread), capRegister);
            /*
             * This updates the real register as opposed to the thread state
             * value. For many architectures, the TLS variables only get
             * updated on a thread switch.
             */
            return Arch_setTLSRegister(tls_base);
        }
#endif
        current_fault = seL4_Fault_UnknownSyscall_new(w);
        handleFault(NODE_STATE(ksCurThread));
    })

    schedule();
    activateThread();

    return EXCEPTION_NONE;
}

exception_t handleUserLevelFault(word_t w_a, word_t w_b)
{
    MCS_DO_IF_BUDGET({
        current_fault = seL4_Fault_UserException_new(w_a, w_b);
        handleFault(NODE_STATE(ksCurThread));
    })
    schedule();
    activateThread();

    return EXCEPTION_NONE;
}

exception_t handleVMFaultEvent(vm_fault_type_t vm_faultType)
{
    MCS_DO_IF_BUDGET({

        exception_t status = handleVMFault(NODE_STATE(ksCurThread), vm_faultType);
        if (status != EXCEPTION_NONE)
        {
            handleFault(NODE_STATE(ksCurThread));
        }
    })

    schedule();
    activateThread();

    return EXCEPTION_NONE;
}

static exception_t handleLongReplyCopy(bool_t read)
{
    tcb_t *thread = NODE_STATE(ksCurThread);
    word_t cap = getRegister(thread, capRegister);
    lookupCap_ret_t lu_ret = lookupCap(NODE_STATE(ksCurThread), cap);
    if (unlikely(lu_ret.status != EXCEPTION_NONE)) {
        userError("Reply cap lookup failed for long message copy");
        current_fault = seL4_Fault_CapFault_new(cap, true);
        handleFault(NODE_STATE(ksCurThread));
        return lu_ret.status;
    }

    if (unlikely(cap_get_capType(lu_ret.cap) != cap_reply_cap)) {
        userError("Long message copy: cap not a reply");
        current_fault = seL4_Fault_CapFault_new(cap, true);
        handleFault(NODE_STATE(ksCurThread));
        lu_ret.status = EXCEPTION_FAULT;
        return lu_ret.status;
    }
    reply_t *reply = REPLY_PTR(cap_reply_cap_get_capReplyPtr(lu_ret.cap));
    return doLongReplyCopy(thread, reply, read);

}

static exception_t handleInvocation(bool_t isCall, bool_t isBlocking, bool_t canDonate, bool_t firstPhase, bool_t isLong, cptr_t cptr)
{
    seL4_MessageInfo_t info;
    lookupCapAndSlot_ret_t lu_ret;
    word_t *buffer;
    exception_t status;
    word_t length;
    tcb_t *thread;

    thread = NODE_STATE(ksCurThread);

    info = messageInfoFromWord(getRegister(thread, msgInfoRegister));

    /* faulting section */
    lu_ret = lookupCapAndSlot(thread, cptr);

    if (unlikely(lu_ret.status != EXCEPTION_NONE)) {
        userError("Invocation of invalid cap #%lu.", cptr);
        current_fault = seL4_Fault_CapFault_new(cptr, false);

        if (isBlocking) {
            handleFault(thread);
        }

        return EXCEPTION_NONE;
    }

    buffer = lookupIPCBuffer(false, thread);

    status = lookupExtraCaps(thread, buffer, info, isLong);

    if (unlikely(status != EXCEPTION_NONE)) {
        userError("Lookup of extra caps failed.");
        if (isBlocking) {
            handleFault(thread);
        }
        return EXCEPTION_NONE;
    }

    if (firstPhase && thread->firstPhaseComplete){
        ipc_debug_printf("handleInvocation: skipping invocation; first phase complete\n");
        return EXCEPTION_NONE;
    }
    /* Syscall error/Preemptible section */
    length = seL4_MessageInfo_get_length(info);
    if (unlikely(length > n_msgRegisters && !buffer)) {
        length = n_msgRegisters;
    }
    status = decodeInvocation(seL4_MessageInfo_get_label(info), length,
                              cptr, lu_ret.slot, lu_ret.cap,
                              isBlocking, isCall,
                              canDonate, isLong, firstPhase, buffer);

    if (unlikely(status == EXCEPTION_PREEMPTED)) {
        return status;
    }

    if (unlikely(status == EXCEPTION_SYSCALL_ERROR)) {
        if (isCall) {
            replyFromKernel_error(thread);
        }
        ipc_debug_printf("handleInvocation: resetting first phase\n");
        thread->firstPhaseComplete = false;
        return status;
    }

    if (unlikely(
            thread_state_get_tsType(thread->tcbState) == ThreadState_Restart)) {
        if (isCall) {
            replyFromKernel_success_empty(thread);
        }
        setThreadState(thread, ThreadState_Running);
    }

    thread->firstPhaseComplete = firstPhase;

    return EXCEPTION_NONE;
}

static inline lookupCap_ret_t lookupReply(void)
{
    word_t replyCPtr = getRegister(NODE_STATE(ksCurThread), replyRegister);
    lookupCap_ret_t lu_ret = lookupCap(NODE_STATE(ksCurThread), replyCPtr);
    if (unlikely(lu_ret.status != EXCEPTION_NONE)) {
        userError("Reply cap lookup failed");
        current_fault = seL4_Fault_CapFault_new(replyCPtr, true);
        handleFault(NODE_STATE(ksCurThread));
        return lu_ret;
    }

    if (unlikely(cap_get_capType(lu_ret.cap) != cap_reply_cap)) {
        userError("Cap in reply slot is not a reply");
        current_fault = seL4_Fault_CapFault_new(replyCPtr, true);
        handleFault(NODE_STATE(ksCurThread));
        lu_ret.status = EXCEPTION_FAULT;
        return lu_ret;
    }

    return lu_ret;
}

static exception_t handleRecv(bool_t isBlocking, bool_t canReply, bool_t isLong, bool_t isPoll)
{
    word_t epCPtr;
    lookupCap_ret_t lu_ret;
    exception_t ipc_ret;

    epCPtr = getRegister(NODE_STATE(ksCurThread), capRegister);

    lu_ret = lookupCap(NODE_STATE(ksCurThread), epCPtr);

    if (unlikely(lu_ret.status != EXCEPTION_NONE)) {
        /* current_lookup_fault has been set by lookupCap */
        current_fault = seL4_Fault_CapFault_new(epCPtr, true);
        handleFault(NODE_STATE(ksCurThread));
        return EXCEPTION_NONE;
    }

    switch (cap_get_capType(lu_ret.cap)) {
    case cap_endpoint_cap:
        if (unlikely(!cap_endpoint_cap_get_capCanReceive(lu_ret.cap))) {
            current_lookup_fault = lookup_fault_missing_capability_new(0);
            current_fault = seL4_Fault_CapFault_new(epCPtr, true);
            handleFault(NODE_STATE(ksCurThread));
            break;
        }

        cap_t ep_cap = lu_ret.cap;
        cap_t reply_cap = cap_null_cap_new();
        if (canReply) {
            lu_ret = lookupReply();
            if (lu_ret.status != EXCEPTION_NONE) {
                return EXCEPTION_NONE;
            } else {
                reply_cap = lu_ret.cap;
            }
        }
        if ((ipc_ret = receiveIPC(NODE_STATE(ksCurThread), ep_cap, isBlocking, isLong, isPoll, reply_cap)) != EXCEPTION_NONE){
            return (ipc_ret);
        }
        break;

    case cap_notification_cap: {
        notification_t *ntfnPtr;
        tcb_t *boundTCB;
        ntfnPtr = NTFN_PTR(cap_notification_cap_get_capNtfnPtr(lu_ret.cap));
        boundTCB = (tcb_t *)notification_ptr_get_ntfnBoundTCB(ntfnPtr);
        if (unlikely(!cap_notification_cap_get_capNtfnCanReceive(lu_ret.cap)
                     || (boundTCB && boundTCB != NODE_STATE(ksCurThread))
                     || isLong)) {
            current_lookup_fault = lookup_fault_missing_capability_new(0);
            current_fault = seL4_Fault_CapFault_new(epCPtr, true);
            handleFault(NODE_STATE(ksCurThread));
            break;
        }

        receiveSignal(NODE_STATE(ksCurThread), lu_ret.cap, isBlocking);
        break;
    }
    case cap_null_cap:
        ipc_debug_printf("attempt to receive from a null cap: %ld\n", epCPtr);
        setRegister(NODE_STATE(ksCurThread), msgInfoRegister, wordFromMessageInfo(seL4_MessageInfo_new(seL4_EndpointCancelled | seL4_IsTransferError, 0, 0, 0)));
        break;
    default:
        current_lookup_fault = lookup_fault_missing_capability_new(0);
        current_fault = seL4_Fault_CapFault_new(epCPtr, true);
        handleFault(NODE_STATE(ksCurThread));
        break;
    }
    return (EXCEPTION_NONE);
}

static inline void mcsPreemptionPoint(irq_t irq)
{
    /* at this point we could be handling a timer interrupt which actually ends the current
     * threads timeslice. However, preemption is possible on revoke, which could have deleted
     * the current thread and/or the current scheduling context, rendering them invalid. */
    if (isSchedulable(NODE_STATE(ksCurThread))) {
        /* if the thread is schedulable, the tcb and scheduling context are still valid */
        checkBudget();
    } else if (NODE_STATE(ksCurSC)->scRefillMax) {
        /* otherwise, if the thread is not schedulable, the SC could be valid - charge it if so */
        chargeBudget(NODE_STATE(ksConsumed), false);
    } else {
        /* If the current SC is no longer configured the time can no
         * longer be charged to it. Simply dropping the consumed time
         * here is equivalent to having charged the consumed time and
         * then having cleared the SC. */
        NODE_STATE(ksConsumed) = 0;
    }
}

static void handleYield(void)
{
    /* Yield the current remaining budget */
    ticks_t consumed = NODE_STATE(ksCurSC)->scConsumed + NODE_STATE(ksConsumed);
    chargeBudget(refill_head(NODE_STATE(ksCurSC))->rAmount, false);
    /* Manually updated the scConsumed so that the full timeslice isn't added, just what was consumed */
    NODE_STATE(ksCurSC)->scConsumed = consumed;
}

static exception_t checkHandleInterrupt(exception_t ret){
    irq_t irq;
    if (unlikely(ret != EXCEPTION_NONE && ret != EXCEPTION_SYSCALL_ERROR)) {
        irq = getActiveIRQ();
        mcsPreemptionPoint(irq);
        if (IRQT_TO_IRQ(irq) != IRQT_TO_IRQ(irqInvalid)) {
            handleInterrupt(irq);
        }
    }
    return (ret);
}

static exception_t checkSecondPhase(exception_t ret){
    if (ret != EXCEPTION_PREEMPTED && ret != EXCEPTION_NONE){
        ipc_debug_printf("checkSecondPhase: resetting first phase\n");
        NODE_STATE(ksCurThread)->firstPhaseComplete = false;
    }
    return (ret);
}

exception_t handleSyscall(syscall_t syscall)
{

    bool_t isLong = false;
    MCS_DO_IF_BUDGET({
        switch (syscall)
        {
        case SysLongSend:
            isLong = true;
        case SysSend:
            checkHandleInterrupt(handleInvocation(false, true, false, false, isLong, getRegister(NODE_STATE(ksCurThread), capRegister)));
            break;
        /*case SysLongNBSend:
            isLong = true;*/
        case SysNBSend:
            checkHandleInterrupt(handleInvocation(false, false, false, false, isLong, getRegister(NODE_STATE(ksCurThread), capRegister)));
            break;
        case SysLongCall:
            isLong = true;
        case SysCall:
            checkHandleInterrupt(handleInvocation(true, true, true, false, isLong, getRegister(NODE_STATE(ksCurThread), capRegister)));
            break;
        case SysLongRecv:
            isLong = true;
        case SysRecv:
            checkHandleInterrupt(handleRecv(true, true, isLong, false));
            break;
        case SysWait:
            checkHandleInterrupt(handleRecv(true, false, false, false));
            break;
        case SysNBWait:
            checkHandleInterrupt(handleRecv(false, false, false, false));
            break;
        case SysPollWait:
            checkHandleInterrupt(handleRecv(false, false, false, true));
            break;
        case SysLongReplyRecv:
            isLong = true;
        case SysReplyRecv: {
            cptr_t reply = getRegister(NODE_STATE(ksCurThread), replyRegister);
            if (checkHandleInterrupt(handleInvocation(false, false, true, true, isLong, reply)) == EXCEPTION_NONE){
                checkHandleInterrupt(checkSecondPhase(handleRecv(true, true, isLong, false)));
            }
            break;
        }
        case SysLongNBSendRecv:
            isLong = true;
        case SysNBSendRecv: {
            cptr_t dest = getNBSendRecvDest();
            checkHandleInterrupt(handleInvocation(false, false, true, true, false, dest));
            checkHandleInterrupt(checkSecondPhase(handleRecv(true, true, isLong, false)));
            break;
        }
        /*case SysLongNBSendWait:
            isLong = true;*/
        case SysNBSendWait:
            checkHandleInterrupt(handleInvocation(false, false, true, true, isLong, getRegister(NODE_STATE(ksCurThread), replyRegister)));
            checkHandleInterrupt(checkSecondPhase(handleRecv(true, false, isLong, false)));
            break;
        /*case SysLongNBRecv:
            isLong = true;*/
        case SysNBRecv:
            checkHandleInterrupt(handleRecv(false, true, isLong, false));
            break;

        case SysLongReadBuf:
            checkHandleInterrupt(handleLongReplyCopy(true));
            break;
        case SysLongWriteBuf:
            checkHandleInterrupt(handleLongReplyCopy(false));
            break;

        case SysYield:
            handleYield();
            break;
        default:
            fail("Invalid syscall");
        }

    })

    schedule();
    activateThread();

    return EXCEPTION_NONE;
}
