/*
 * Copyright 2014, General Dynamics C4 Systems
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <types.h>
#include <sel4/mode/types.h>
#include <string.h>
#include <sel4/constants.h>
#include <kernel/thread.h>
#include <kernel/vspace.h>
#include <machine/registerset.h>
#include <model/statedata.h>
#include <object/notification.h>
#include <object/cnode.h>
#include <object/endpoint.h>
#include <object/tcb.h>

static inline void ep_ptr_set_queue(endpoint_t *epptr, tcb_queue_t queue)
{
    endpoint_ptr_set_epQueue_head(epptr, (word_t)queue.head);
    endpoint_ptr_set_epQueue_tail(epptr, (word_t)queue.end);
}

static exception_t sendIPCInner(bool_t blocking, bool_t do_call, word_t badge,
             bool_t canGrant, bool_t canGrantReply, bool_t canDonate,
             bool_t isLong, bool_t firstPhase, tcb_t *thread, endpoint_t *epptr,
             bool_t *finished)
{
    exception_t status = EXCEPTION_NONE;

    ipc_debug_printf("sendIPC: %p\n", thread);

    endpoint_state_t epState = endpoint_ptr_get_state(epptr);

    bool_t isFault = seL4_Fault_get_seL4_FaultType(thread->tcbFault) != seL4_Fault_NullFault;

    if (thread->tcbIPCPartner != NULL && !isFault){
        epState = EPState_Recv;
    }

    switch (epState) {
    case EPState_Idle:
    case EPState_Send:
        if (blocking) {
            tcb_queue_t queue;

            /* Set thread state to BlockedOnSend */
            thread_state_ptr_set_tsType(&thread->tcbState,
                                        ThreadState_BlockedOnSend);
            thread_state_ptr_set_blockingObject(
                &thread->tcbState, EP_REF(epptr));
            thread_state_ptr_set_blockingIPCBadge(
                &thread->tcbState, badge);
            thread_state_ptr_set_blockingIPCCanGrant(
                &thread->tcbState, canGrant);
            thread_state_ptr_set_blockingIPCCanGrantReply(
                &thread->tcbState, canGrantReply);
            thread_state_ptr_set_blockingIPCIsCall(
                &thread->tcbState, do_call);
            thread_state_ptr_set_firstPhase(
                &thread->tcbState, firstPhase);
            scheduleTCB(thread);

            /* Place calling thread in endpoint queue */
            queue = ep_ptr_get_queue(epptr);
            queue = tcbEPAppend(thread, queue);
            endpoint_ptr_set_state(epptr, EPState_Send);
            ep_ptr_set_queue(epptr, queue);
            if (isLong && thread->longIPCState == LongIPCState_Inactive){
                thread->longIPCState = LongIPCState_Blocked;
                thread->longIPCFlags = seL4_MessageInfo_get_capsUnwrapped(messageInfoFromWord(getRegister(thread, msgInfoRegister)));
            }
        }
        *finished = true;
        break;

    case EPState_Recv: {
        tcb_queue_t queue;
        tcb_t *dest;

        if (thread->tcbIPCPartner != NULL && !isFault){
            dest = thread->tcbIPCPartner;
        }else{
            /* Get the head of the endpoint queue. */
            queue = ep_ptr_get_queue(epptr);
            dest = queue.head;

            /* Haskell error "Receive endpoint queue must not be empty" */
            assert(dest);

            /* Dequeue the first TCB */
            queue = tcbEPDequeue(dest, queue);
            ep_ptr_set_queue(epptr, queue);

            if (!queue.head) {
                endpoint_ptr_set_state(epptr, EPState_Idle);
            }
        }

        if (isLong && thread->longIPCState == LongIPCState_Inactive && dest->longIPCState == LongIPCState_Blocked){
            thread->longIPCState = LongIPCState_Blocked;
            thread->longIPCFlags = seL4_MessageInfo_get_capsUnwrapped(messageInfoFromWord(getRegister(thread, msgInfoRegister)));
        }

        bool_t receiveIsLong = dest->longIPCState != LongIPCState_Inactive;
        if (dest->blockingIPCIsPoll) {
            status = preemptionPoint();
        }else{
            /* Do the transfer */
            status = doIPCTransfer(true, true, true, do_call, thread, &thread->primaryLongVector, epptr, badge, canGrant, dest, &dest->primaryLongVector, &thread->replyLongVector);
            *finished = true;
        }
        if (status == EXCEPTION_SYSCALL_ERROR){
            transferFailed(thread);
            do_call = false;
        }else if (status != EXCEPTION_NONE){
            return (status);
        }else if (receiveIsLong){
            ipc_debug_printf("setting long vector info: %ld %ld\n", dest->primaryLongVector.maxTransferred, thread->primaryLongVector.maxTransferred);
            void *receiveBuffer = lookupIPCBuffer(true, dest);
            setLongVectorInfo(receiveBuffer, LongBuf_ReceiveSize, dest->primaryLongVector.maxTransferred);
            setLongVectorInfo(receiveBuffer, LongBuf_ReceiveTotalSize, thread->primaryLongVector.maxTransferred);
            if (do_call) {
                ipc_debug_printf("setting reply long vector info: %ld\n", thread->replyLongVector.maxTransferred);
                setLongVectorInfo(receiveBuffer, LongBuf_ReceiveReplySize, thread->replyLongVector.maxTransferred);
            }
        }

        if (!isFault){
            word_t replyVectorAddress = thread->replyLongVector.address;
            word_t replyVectorCount = thread->replyLongVector.count;
            ipc_debug_printf("send done\n");
            if (do_call){
                thread->replyLongVector.maxTransferred = 0;
            }else{
                ipc_debug_printf("sendIPC: resetting sender vectors\n");
                resetLongVectors(thread);
            }
            if (firstPhase){
                thread->primaryLongVector.address = replyVectorAddress;
                thread->primaryLongVector.count = replyVectorCount;
            }
            ipc_debug_printf("sendIPC: resetting receiver vectors\n");
            resetLongVectors(dest);
            ipc_debug_printf("sendIPC: resetting first phase\n");
            dest->firstPhaseComplete = false;
            if (!do_call){
                replyFromKernel_success_empty_send(thread);
            }
        }

        reply_t *reply = REPLY_PTR(thread_state_get_replyObject(dest->tcbState));
        if (reply) {
            reply_unlink(reply, dest);
        }

        if (do_call ||
            seL4_Fault_ptr_get_seL4_FaultType(&thread->tcbFault) != seL4_Fault_NullFault) {
            if (reply != NULL && (canGrant || canGrantReply)) {
                reply_push(thread, dest, reply, canDonate);
            } else {
                setThreadState(thread, ThreadState_Inactive);
            }
        } else if (canDonate && dest->tcbSchedContext == NULL) {
            schedContext_donate(thread->tcbSchedContext, dest);
        }
        checkPause(thread, false);

        /* blocked threads should have enough budget to get out of the kernel */
        assert(dest->tcbSchedContext == NULL || refill_sufficient(dest->tcbSchedContext, 0));
        assert(dest->tcbSchedContext == NULL || refill_ready(dest->tcbSchedContext));
        bool_t destPaused = checkPause(dest, true);
        if (!destPaused) {
            setThreadState(dest, ThreadState_Running);
        }
        if (sc_sporadic(dest->tcbSchedContext) && dest->tcbSchedContext != NODE_STATE(ksCurSC)) {
            refill_unblock_check(dest->tcbSchedContext);
        }
        if (!destPaused) {
            possibleSwitchTo(dest);
        }
        break;
    }
    }
    return (status);
}

exception_t sendIPC(bool_t blocking, bool_t do_call, word_t badge,
             bool_t canGrant, bool_t canGrantReply, bool_t canDonate,
             bool_t isLong, bool_t firstPhase, tcb_t *thread, endpoint_t *epptr)
{
    bool_t finished = false;
    bool_t status = EXCEPTION_NONE;
    do {
        status = sendIPCInner(blocking, do_call, badge, canGrant, canGrantReply, canDonate, isLong, firstPhase, thread, epptr, &finished);
    } while (!finished && status == EXCEPTION_NONE);
    return status;
}

exception_t receiveIPC(tcb_t *thread, cap_t cap, bool_t isBlocking,
        bool_t isLong, bool_t isPoll, cap_t replyCap)
{
    endpoint_t *epptr;
    notification_t *ntfnPtr;
    exception_t status = EXCEPTION_NONE;

    ipc_debug_printf("receiveIPC: %p\n", thread);
    /* Haskell error "receiveIPC: invalid cap" */
    assert(cap_get_capType(cap) == cap_endpoint_cap);

    epptr = EP_PTR(cap_endpoint_cap_get_capEPPtr(cap));

    reply_t *replyPtr = NULL;
    if (cap_get_capType(replyCap) == cap_reply_cap) {
        replyPtr = REPLY_PTR(cap_reply_cap_get_capReplyPtr(replyCap));
        if (unlikely(replyPtr->replyTCB != NULL && thread->tcbIPCPartner == NULL)) {
            //If the reply object already has a pending reply and tcbIPCPartner
            //is non-null, this was an attempt to receive a message into a reply
            //object with a reply already pending, and not a retry after a
            //preemption.
            current_syscall_error.type = seL4_ReplySequenceError;
            transferFailed(thread);
            return EXCEPTION_SYSCALL_ERROR;
        }
    }

    /* Check for anything waiting in the notification */
    ntfnPtr = thread->tcbBoundNotification;
    endpoint_state_t epState = endpoint_ptr_get_state(epptr);
    notification_state_t ntfnState = NtfnState_Idle;

    if (thread->tcbIPCPartner){
        epState = EPState_Send;
    }else if (ntfnPtr && !isPoll){
        ntfnState = notification_ptr_get_state(ntfnPtr);
    }

    if (ntfnPtr && ntfnState == NtfnState_Active) {
        completeSignal(ntfnPtr, thread);
        thread->firstPhaseComplete = false;
    } else {
        /* If this is a blocking recv and we didn't have a pending notification,
         * then if we are running on an SC from a bound notification, then we
         * need to return it so that we can passively wait on the EP for potentially
         * SC donations from client threads.
         */
        if (ntfnPtr && isBlocking) {
            maybeReturnSchedContext(ntfnPtr, thread);
        }
        switch (epState) {
        case EPState_Idle:
        case EPState_Recv: {
            tcb_queue_t queue;
            if (isBlocking) {
                /* Set thread state to BlockedOnReceive */
                thread_state_ptr_set_tsType(&thread->tcbState,
                                            ThreadState_BlockedOnReceive);
                thread_state_ptr_set_blockingObject(
                    &thread->tcbState, EP_REF(epptr));
                thread_state_ptr_set_replyObject(&thread->tcbState, REPLY_REF(replyPtr));
                if (replyPtr) {
                    replyPtr->replyTCB = thread;
                }
                scheduleTCB(thread);

                /* Place calling thread in endpoint queue */
                queue = ep_ptr_get_queue(epptr);
                queue = tcbEPAppend(thread, queue);
                endpoint_ptr_set_state(epptr, EPState_Recv);
                ep_ptr_set_queue(epptr, queue);
                if (isLong && thread->longIPCState == LongIPCState_Inactive){
                    thread->longIPCState = LongIPCState_Blocked;
                    thread->longIPCFlags = seL4_MessageInfo_get_capsUnwrapped(messageInfoFromWord(getRegister(thread, msgInfoRegister)));
                }
                thread->blockingIPCIsPoll = isPoll;
            } else {
                doNBRecvFailedTransfer(thread);
            }
            break;
        }

        case EPState_Send: {
            tcb_queue_t queue;
            tcb_t *sender;
            word_t badge;
            bool_t canGrant;
            bool_t canGrantReply;
            bool_t do_call;

            if (isPoll) {
                return EXCEPTION_NONE;
            }

            if (thread->tcbIPCPartner != NULL){
                sender = thread->tcbIPCPartner;
            }else{
                /* Get the head of the endpoint queue. */
                queue = ep_ptr_get_queue(epptr);
                sender = queue.head;

                /* Haskell error "Send endpoint queue must not be empty" */
                assert(sender);

                /* Dequeue the first TCB */
                queue = tcbEPDequeue(sender, queue);
                ep_ptr_set_queue(epptr, queue);

                if (!queue.head) {
                    endpoint_ptr_set_state(epptr, EPState_Idle);
                }
            }
            if (isLong && thread->longIPCState == LongIPCState_Inactive && sender->longIPCState == LongIPCState_Blocked){
                thread->longIPCState = LongIPCState_Blocked;
                thread->longIPCFlags = seL4_MessageInfo_get_capsUnwrapped(messageInfoFromWord(getRegister(thread, msgInfoRegister)));
            }

            /* Get sender IPC details */
            badge = thread_state_ptr_get_blockingIPCBadge(&sender->tcbState);
            canGrant =
                thread_state_ptr_get_blockingIPCCanGrant(&sender->tcbState);
            canGrantReply =
                thread_state_ptr_get_blockingIPCCanGrantReply(&sender->tcbState);

            if (isLong){
                setThreadState(NODE_STATE(ksCurThread), ThreadState_Restart);
            }

            do_call = thread_state_ptr_get_blockingIPCIsCall(&sender->tcbState);

            /* Do the transfer */
            status = doIPCTransfer(false, true, true, do_call, sender,
                          &sender->primaryLongVector, epptr, badge, canGrant,
                          thread, &thread->primaryLongVector,
                          &sender->replyLongVector);
            if (status != EXCEPTION_NONE && status != EXCEPTION_SYSCALL_ERROR){
                return status;
            }else if (isLong){
                void *receiveBuffer = lookupIPCBuffer(true, thread);
                ipc_debug_printf("setting long vector info: %ld %ld\n", thread->primaryLongVector.maxTransferred, sender->primaryLongVector.maxTransferred);
                setLongVectorInfo(receiveBuffer, LongBuf_ReceiveSize, thread->primaryLongVector.maxTransferred);
                setLongVectorInfo(receiveBuffer, LongBuf_ReceiveTotalSize, sender->primaryLongVector.maxTransferred);
                if (do_call) {
                    ipc_debug_printf("setting reply long vector info: %ld\n", thread->replyLongVector.maxTransferred);
                    setLongVectorInfo(receiveBuffer, LongBuf_ReceiveReplySize, sender->replyLongVector.maxTransferred);
                }
            }
            ipc_debug_printf("receiveIPC: resetting first phase: endpoint\n");
            thread->firstPhaseComplete = false;

            if (isLong){
                setThreadState(NODE_STATE(ksCurThread), ThreadState_Running);
            }

            if (status == EXCEPTION_SYSCALL_ERROR){
                transferFailed(sender);
                do_call = false;
            }else if (!do_call){
                replyFromKernel_success_empty_send(sender);
            }

            bool_t senderFault = seL4_Fault_get_seL4_FaultType(sender->tcbFault) != seL4_Fault_NullFault;

            word_t replyVectorAddress = thread->replyLongVector.address;
            word_t replyVectorCount = thread->replyLongVector.count;
            ipc_debug_printf("receive done\n");
            if (do_call){
                sender->replyLongVector.maxTransferred = 0;
            }else if (!senderFault){
                ipc_debug_printf("receiveIPC: resetting sender vectors\n");
                resetLongVectors(sender);
            }
            bool_t firstPhase = thread_state_ptr_get_firstPhase(&sender->tcbState);

            if (firstPhase && !senderFault){
                thread->primaryLongVector.address = replyVectorAddress;
                thread->primaryLongVector.count = replyVectorCount;
            }
            ipc_debug_printf("receiveIPC: resetting receiver vectors\n");
            resetLongVectors(thread);
            if (sc_sporadic(sender->tcbSchedContext)) {
                /* We know that the sender can't have the current SC as
                 * its own SC as this point as it should still be
                 * associated with the current thread, no thread, or a
                 * thread that isn't blocked. This check is added here
                 * to reduce the cost of proving this to be true as a
                 * short-term stop-gap. */
                assert(sender->tcbSchedContext != NODE_STATE(ksCurSC));
                if (sender->tcbSchedContext != NODE_STATE(ksCurSC)) {
                    refill_unblock_check(sender->tcbSchedContext);
                }
            }


            if (do_call || senderFault) {
                if ((canGrant || canGrantReply) && replyPtr != NULL) {
                    bool_t canDonate = sender->tcbSchedContext != NULL
                                       && seL4_Fault_get_seL4_FaultType(sender->tcbFault) != seL4_Fault_Timeout;
                    reply_push(sender, thread, replyPtr, canDonate);
                    checkPause(sender, false);
                } else if (!checkPause(sender, false)){
                    setThreadState(sender, ThreadState_Inactive);
                }
                if (!senderFault){
                    replyFromKernel_success_empty_send(sender);
                }

            } else if (!checkPause(sender, false)) {
                setThreadState(sender, ThreadState_Running);
                possibleSwitchTo(sender);
                assert(sender->tcbSchedContext == NULL || refill_sufficient(sender->tcbSchedContext, 0));
            }
            checkPause(thread, false);
            break;
        }
        }
    }
    return status;
}

void replyFromKernel_error(tcb_t *thread)
{
    word_t len;
    word_t *ipcBuffer;

    ipcBuffer = lookupIPCBuffer(true, thread);
    setRegister(thread, badgeRegister, 0);
    len = setMRs_syscall_error(thread, ipcBuffer);

#ifdef CONFIG_KERNEL_INVOCATION_REPORT_ERROR_IPC
    char *debugBuffer = (char *)(ipcBuffer + DEBUG_MESSAGE_START + 1);
    word_t add = strlcpy(debugBuffer, (char *)current_debug_error.errorMessage,
                         DEBUG_MESSAGE_MAXLEN * sizeof(word_t));

    len += (add / sizeof(word_t)) + 1;
#endif

    seL4_Word transferError = 0;
    if (current_syscall_error.type > seL4_MinTransferError && current_syscall_error.type < seL4_MaxTransferError) {
        transferError = seL4_IsTransferError;
    }

    ipc_debug_printf("replyFromKernel_error: setting info register\n");
    setRegister(thread, msgInfoRegister, wordFromMessageInfo(
                    seL4_MessageInfo_new(current_syscall_error.type | transferError, 0, 0, len)));
}

void transferFailed(tcb_t *thread)
{
    if (current_syscall_error.type < seL4_MinTransferError || current_syscall_error.type > seL4_MaxTransferError) {
        fail("Invalid transfer error");
    }

    ipc_debug_printf("transferFailed: %p %ld\n", thread, current_syscall_error.type);
    setRegister(thread, msgInfoRegister, wordFromMessageInfo(
                    seL4_MessageInfo_new(current_syscall_error.type | seL4_IsTransferError, 0, 0, 0)));
}

void replyFromKernel_success_empty_send(tcb_t *thread)
{
    ipc_debug_printf("replyFromKernel_success_empty_send\n");
    setRegister(thread, msgInfoRegister, wordFromMessageInfo(
                    seL4_MessageInfo_new(0, 0, 0, 0)));
}

void replyFromKernel_success_empty(tcb_t *thread)
{
    setRegister(thread, badgeRegister, 0);
    setRegister(thread, msgInfoRegister, wordFromMessageInfo(
                    seL4_MessageInfo_new(0, 0, 0, 0)));
}

static void baseCancelIPC(tcb_t *tptr, bool_t forceReset)
{
    thread_state_t *state = &tptr->tcbState;
    /* cancel ipc cancels all faults */
    seL4_Fault_NullFault_ptr_new(&tptr->tcbFault);

    word_t tsType = thread_state_ptr_get_tsType(state);
    ipc_debug_printf("baseCancelIPC %p %ld\n", tptr, tsType);
    if (forceReset){
        tcb_t *IPCPartner = tptr->tcbIPCPartner;
        switch (tptr->longIPCState){
        case LongIPCState_CopyRunning:
        case LongIPCState_CopyBlocked:
            ipc_debug_printf("cancelling copy in progress\n");
            IPCPartner->longIPCState = LongIPCState_Inactive;
            cancelIPC(IPCPartner);
            setRegister(IPCPartner, msgInfoRegister, wordFromMessageInfo(
                seL4_MessageInfo_new(seL4_ThreadCancelled | seL4_IsTransferError, 0, 0, 0)));
            setRegister(tptr, msgInfoRegister, wordFromMessageInfo(
                seL4_MessageInfo_new(seL4_ThreadCancelled | seL4_IsTransferError, 0, 0, 0)));
            setThreadState(IPCPartner, ThreadState_Running);
        default:
            ipc_debug_printf("cancelIPC: resetting vectors: %ld %ld\n", tptr->firstPhaseComplete, tsType);
            resetLongVectors(tptr);
            resetLongIPCState(tptr);
        }
    }
    switch (tsType) {
    case ThreadState_BlockedOnSend:
    case ThreadState_BlockedOnReceive: {
        ipc_debug_printf("cancelling send/receive\n");
        /* blockedIPCCancel state */
        endpoint_t *epptr;
        tcb_queue_t queue;

        epptr = EP_PTR(thread_state_ptr_get_blockingObject(state));

        if (endpoint_ptr_get_state(epptr) != EPState_Idle){
            /* Dequeue TCB */
            queue = ep_ptr_get_queue(epptr);
            queue = tcbEPDequeue(tptr, queue);
            ep_ptr_set_queue(epptr, queue);

            if (!queue.head) {
                endpoint_ptr_set_state(epptr, EPState_Idle);
            }
        }
        reply_t *reply = REPLY_PTR(thread_state_get_replyObject(tptr->tcbState));
        if (reply != NULL) {
            reply_unlink(reply, tptr);
        }
        tptr->tcbIPCPartner = NULL;
        tptr->tcbReplySchedContext = NULL;
        tptr->tcbReplySchedContextDest = NULL;
        setThreadState(tptr, ThreadState_Inactive);
        break;
    }

    case ThreadState_BlockedOnNotification:
        ipc_debug_printf("cancelling notification\n");
        cancelSignal(tptr,
                     NTFN_PTR(thread_state_ptr_get_blockingObject(state)));
        break;

    case ThreadState_BlockedOnReply: {
        ipc_debug_printf("cancelling reply\n");
        reply_remove_tcb(tptr);

        break;
    }
    }
}

bool_t pauseIPC(tcb_t *tptr)
{
    ipc_debug_printf("pauseIPC %p\n", tptr);
    thread_state_t *state = &tptr->tcbState;

    switch (tptr->longIPCState){
    case LongIPCState_CopyRunning:
    case LongIPCState_CopyBlocked:
        ipc_debug_printf("pausing long transfer\n");
        tptr->pauseOnIPCCompletion = true;
        return false;
    }
    seL4_Fault_NullFault_ptr_new(&tptr->tcbFault);
    word_t tsType = thread_state_ptr_get_tsType(state);
    switch (tsType) {
    case ThreadState_BlockedOnSend:
    case ThreadState_BlockedOnReceive:
    case ThreadState_BlockedOnNotification:
        softCancelIPC(tptr);
        break;
    case ThreadState_BlockedOnReply:
        ipc_debug_printf("pausing reply\n");
        tptr->pauseOnIPCCompletion = true;
        return false;
    }
    return true;
}

void cancelIPC(tcb_t *tptr)
{
    baseCancelIPC(tptr, true);
}

void softCancelIPC(tcb_t *tptr)
{
    baseCancelIPC(tptr, false);
}

void cancelAllIPC(endpoint_t *epptr)
{
    ipc_debug_printf("cancelAllIPC\n");
    switch (endpoint_ptr_get_state(epptr)) {
    case EPState_Idle:
        break;

    default: {
        tcb_t *thread = TCB_PTR(endpoint_ptr_get_epQueue_head(epptr));

        /* Make endpoint idle */
        endpoint_ptr_set_state(epptr, EPState_Idle);
        endpoint_ptr_set_epQueue_head(epptr, 0);
        endpoint_ptr_set_epQueue_tail(epptr, 0);

        /* Set all blocked threads to restart */
        for (; thread; thread = thread->tcbEPNext) {
            //TODO: test cancellation of in-progress long IPC more extensively
            thread->longIPCState = LongIPCState_Inactive;
            resetLongVectors(thread);
            resetLongIPCState(thread);

            reply_t *reply = REPLY_PTR(thread_state_get_replyObject(thread->tcbState));
            if (reply != NULL) {
                reply_unlink(reply, thread);
            }
            if (seL4_Fault_get_seL4_FaultType(thread->tcbFault) == seL4_Fault_NullFault) {
                ipc_debug_printf("cancelAllIPC: no fault\n");
                setRegister(thread, msgInfoRegister, wordFromMessageInfo(
                    seL4_MessageInfo_new(seL4_EndpointCancelled | seL4_IsTransferError, 0, 0, 0)));
                thread->firstPhaseComplete = false;
                setThreadState(thread, ThreadState_Running);

                if (sc_sporadic(thread->tcbSchedContext)) {
                    /* We know that the thread can't have the current SC
                     * as its own SC as this point as it should still be
                     * associated with the current thread, or no thread.
                     * This check is added here to reduce the cost of
                     * proving this to be true as a short-term stop-gap. */
                    assert(thread->tcbSchedContext != NODE_STATE(ksCurSC));
                    if (thread->tcbSchedContext != NODE_STATE(ksCurSC)) {
                        refill_unblock_check(thread->tcbSchedContext);
                    }
                }
                possibleSwitchTo(thread);
            } else {
                setThreadState(thread, ThreadState_Inactive);
            }
        }

        rescheduleRequired();
        break;
    }
    }
}

void cancelBadgedSends(endpoint_t *epptr, word_t badge)
{
    switch (endpoint_ptr_get_state(epptr)) {
    case EPState_Idle:
    case EPState_Recv:
        break;

    case EPState_Send: {
        tcb_t *thread, *next;
        tcb_queue_t queue = ep_ptr_get_queue(epptr);

        /* this is a de-optimisation for verification
         * reasons. it allows the contents of the endpoint
         * queue to be ignored during the for loop. */
        endpoint_ptr_set_state(epptr, EPState_Idle);
        endpoint_ptr_set_epQueue_head(epptr, 0);
        endpoint_ptr_set_epQueue_tail(epptr, 0);

        for (thread = queue.head; thread; thread = next) {
            word_t b = thread_state_ptr_get_blockingIPCBadge(
                           &thread->tcbState);
            next = thread->tcbEPNext;
            /* senders do not have reply objects in their state, and we are only cancelling sends */
            assert(REPLY_PTR(thread_state_get_replyObject(thread->tcbState)) == NULL);
            if (b == badge) {
                thread->longIPCState = LongIPCState_Inactive;
                resetLongVectors(thread);
                resetLongIPCState(thread);
                if (seL4_Fault_get_seL4_FaultType(thread->tcbFault) ==
                    seL4_Fault_NullFault) {
                    setThreadState(thread, ThreadState_Restart);
                    if (sc_sporadic(thread->tcbSchedContext)) {
                        /* We know that the thread can't have the current SC
                         * as its own SC as this point as it should still be
                         * associated with the current thread, or no thread.
                         * This check is added here to reduce the cost of
                         * proving this to be true as a short-term stop-gap. */
                        assert(thread->tcbSchedContext != NODE_STATE(ksCurSC));
                        if (thread->tcbSchedContext != NODE_STATE(ksCurSC)) {
                            refill_unblock_check(thread->tcbSchedContext);
                        }
                    }
                    possibleSwitchTo(thread);
                } else {
                    setThreadState(thread, ThreadState_Inactive);
                }
                queue = tcbEPDequeue(thread, queue);
            }
        }
        ep_ptr_set_queue(epptr, queue);

        if (queue.head) {
            endpoint_ptr_set_state(epptr, EPState_Send);
        }

        rescheduleRequired();

        break;
    }

    default:
        fail("invalid EP state");
    }
}

void reorderEP(endpoint_t *epptr, tcb_t *thread)
{
    tcb_queue_t queue = ep_ptr_get_queue(epptr);
    queue = tcbEPDequeue(thread, queue);
    queue = tcbEPAppend(thread, queue);
    ep_ptr_set_queue(epptr, queue);
}
